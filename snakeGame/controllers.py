import pygame, random



def foodSpawn():
    foodPos =   [[random.randint(0,49)*10, random.randint(0,49)*10],
                    [random.randint(0,49)*10, random.randint(0,49)*10],
                    [random.randint(0,49)*10, random.randint(0,49)*10],
                    [random.randint(0,49)*10, random.randint(0,49)*10],
                    [random.randint(0,49)*10, random.randint(0,49)*10]
                ]
    return foodPos


def eventMove(event):
    if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    return "RIGHT"
                if event.key == pygame.K_LEFT:
                    return "LEFT"
                if event.key == pygame.K_UP:
                   return "UP"
                if event.key == pygame.K_DOWN:
                   return "DOWN"

def toLose(snakePos, snakeBody,score):
    if snakePos[0] > 710 or snakePos[0] < 0 or snakePos[1] > 500 or snakePos[1] < 0:
        print(f"Game over Score: {score}")
        return False

    if snakePos in snakeBody[1:]:
        print(f"Game over Score: {score}")
        return False
    
    return True

def render(playSurface,snakeBody,foodPos):
    playSurface.fill((32, 32, 32))

    for pos in snakeBody:
        pygame.draw.rect(playSurface, (0, 255, 0), pygame.Rect(pos[0], pos[1], 10, 10))

    for food in foodPos:
        pygame.draw.rect(playSurface, (250, 250, 250), pygame.Rect(food[0], food[1], 10, 10))

    pygame.display.update()
