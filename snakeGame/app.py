
import pygame
from pygame.locals import *

from controllers import *

pygame.init()

playSurface = pygame.display.set_mode((720, 500))

fps = pygame.time.Clock()


def main():

    snakePos = [100, 50]
    snakeBody = [[100, 50], [90, 50], [80, 50], [70, 50]]
    direction = "RIGHT"
    changeTo = direction

    foodPos = foodSpawn()
    score = 0

    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            changeTo = eventMove(event)
        

        if changeTo == "RIGHT" and direction != "LEFT":
            direction = "RIGHT"
        if changeTo == "LEFT" and direction != "RIGHT":
            direction = "LEFT"
        if changeTo == "UP" and direction != "DOWN":
            direction = "UP"
        if changeTo == "DOWN" and direction != "UP":
            direction = "DOWN"

        if direction == "RIGHT":
            snakePos[0] += 10
        if direction == "LEFT":
            snakePos[0] -= 10
        if direction == "UP":
            snakePos[1] -= 10
        if direction == "DOWN":
            snakePos[1] += 10

        snakeBody.insert(0, list(snakePos))

        insert = False

        if snakePos == foodPos[0] or snakePos == foodPos[1] or snakePos == foodPos[2] or snakePos == foodPos[3] or snakePos == foodPos[4]:
            score += 100
            foodPos = foodSpawn()

            insert = True

        else:
            if insert == True:
                return
            else:
                snakeBody.pop()


        render(playSurface,snakeBody,foodPos)

        

        run = toLose(snakePos, snakeBody,score)

        
        fps.tick(30)

main()
pygame.quit()

